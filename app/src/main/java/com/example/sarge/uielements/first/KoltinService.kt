package com.example.sarge.uielements.first

import android.app.IntentService
import android.content.Intent
import android.content.IntentSender
import android.util.Log
import com.activeandroid.ActiveAndroid

import com.example.sarge.uielements.entity.Item



/**
 * Created by sarge on 18.01.2017.
 */

class KoltinService: IntentService("test") {

    override fun onCreate() {
        super.onCreate()
    }

    override fun onHandleIntent(p0: Intent?) {
        ActiveAndroid.beginTransaction()
        try {
            when(p0!!.getStringExtra("type")) {
                "delete" -> Item.deleteAll()
                "new" -> add()
                "size" -> Item.getDbSize()
            }
            ActiveAndroid.setTransactionSuccessful()
        } finally {
            ActiveAndroid.endTransaction()
        }
    }

    fun add() {
        for (i in 0..10000) {
            val item = Item(i.toString())
            item.save()
            Log.d("Logos", "save " + i)
        }
    }


}
