package com.example.sarge.uielements.second

import android.app.Activity.RESULT_OK
import android.app.IntentService
import android.content.Intent
import android.os.Bundle
import android.support.v4.os.ResultReceiver
import android.util.Log
import android.support.v4.content.LocalBroadcastManager
import android.app.Activity
import android.app.Notification


/**
 * Created by sarge on 19.01.2017.
 */

class TestService: IntentService("second") {

    companion object {
        val ACTION = "com.codepath.example.servicesdemo.MyTestService"
    }

    override fun onCreate() {
        super.onCreate()
    }

    override fun onHandleIntent(intent: Intent?) {
        when (intent!!.getStringExtra("type")) {
            "receiver" -> secondReceiver(intent)
            "broadcast" -> secondBroadcast()
        }
    }

    fun secondReceiver(intent: Intent?) {
        Log.d("Logos", "onHandleIntent begin " + intent!!.hasExtra("receiver"))
        var resultReceiver: ResultReceiver? = null
        try {
            Log.d("Logos", "onHandleIntent try")
            resultReceiver = intent.getParcelableExtra<ResultReceiver>("receiver")
        }
        catch (e: Exception) {
            Log.d("Logos", "onHandleIntent Exception")
            e.printStackTrace()
        }
        val bundle: Bundle = Bundle()
        bundle.putString("resultValue", "My Result Value. Passed in: ")
        resultReceiver?.send(RESULT_OK, bundle)
        Log.d("Logos", "onHandleIntent end")
    }

    fun secondBroadcast() {
        Log.d("Logos", "onHandleIntent secondBroadcast begin")
        // Construct an Intent tying it to the ACTION (arbitrary event namespace)
        val intent = Intent(ACTION)
        // Put extras into the intent as usual
        intent.putExtra("resultCode", Activity.RESULT_OK)
        intent.putExtra("resultValue", "My Result Value. Passed in: ")
        // Fire the broadcast with intent packaged
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
        // or sendBroadcast(in) for a normal broadcast;
        Log.d("Logos", "onHandleIntent secondBroadcast end")
    }

}
