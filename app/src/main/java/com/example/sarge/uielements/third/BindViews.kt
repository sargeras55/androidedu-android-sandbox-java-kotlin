package com.example.sarge.uielements.third

import android.app.Activity
import android.support.annotation.UiThread

/**
 * Created by sarge on 22.01.2017.
 */

@UiThread
fun bindViewsFromActivity(activity: Activity) {
    val mClass = activity.javaClass
    val mAnnotation = onClick::class.java
    mClass.methods.filter { it.isAnnotationPresent(mAnnotation) }
            .forEach {
                val viewId: Int = it.getAnnotation(mAnnotation).res // viewId from annotation parameter
                val view = activity.findViewById(viewId)
                val method = it
                view.setOnClickListener {
                    method.invoke(activity)
                }
            }
}
