package com.example.sarge.uielements.second

import android.app.Activity
import android.app.Notification
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.example.sarge.uielements.R
import kotlinx.android.synthetic.main.activity_second.*
import android.support.v4.content.LocalBroadcastManager
import android.content.IntentFilter
import android.content.BroadcastReceiver
import android.content.Context
import android.app.NotificationManager
import android.app.PendingIntent
import android.support.v4.app.NotificationCompat
import com.example.sarge.uielements.entity.Item


class SecondActivity : AppCompatActivity() {

    private var testResultReceiver: TestResultReceiver? = null
    private val NOTIFY_ID = 101


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        setupReceiver()
        secondReceiver.setOnClickListener { startService() }
        secondBroadcast.setOnClickListener { startBroadcast() }
        secondNotification.setOnClickListener { sendNotification() }
    }


    override fun onResume() {
        super.onResume()
        val filter = IntentFilter(TestService.ACTION)
        LocalBroadcastManager.getInstance(this).registerReceiver(testReceiver, filter)
    }


    override fun onPause() {
        super.onPause()
        // Unregister the listener when the application is paused
        LocalBroadcastManager.getInstance(this).unregisterReceiver(testReceiver)
        // or `unregisterReceiver(testReceiver)` for a normal broadcast
    }

    override fun startLockTask() {
        super.startLockTask()
    }

    fun startService() {
        Log.d("Logos", "startService")
        startService(Intent(this, TestService::class.java)
                .putExtra("receiver", testResultReceiver)
                .putExtra("type", "receiver"))
    }


    fun startBroadcast() {
        Log.d("Logos", "startService")
        startService(Intent(this, TestService::class.java)
                .putExtra("type", "broadcast"))
    }


    fun sendNotification() {
        val intent = Intent()
        val pIntent = PendingIntent.getActivity(this, 0, intent, 0)
        val message = "Unregister the listener when the application is paused"
        val action = NotificationCompat.Action.Builder(R.mipmap.ic_launcher, "Previous", pIntent).build()

        val notification = NotificationCompat.Builder(baseContext)
                .setContentTitle("Test Notification")
                .setTicker("Test Ticker")
                .setStyle(NotificationCompat.BigTextStyle().bigText(message))
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentText(message)
                .addAction(action)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .build()

        val nm: NotificationManager = baseContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nm.notify(NOTIFY_ID, notification)
    }


    fun setupReceiver() {
        Log.d("Logos", "setupReceiver")
        testResultReceiver = TestResultReceiver(Handler())
        testResultReceiver!!.receiver = object : TestResultReceiver.Receiver {
            override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
                Log.d("Logos", "onReceiveResult")
                if (resultCode == Activity.RESULT_OK) {
                    val resultValue = resultData?.getString("resultValue")
                    Toast.makeText(baseContext, resultValue, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }


    private val testReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val resultCode = intent.getIntExtra("resultCode", Activity.RESULT_CANCELED)
            Log.d("Logos", "testReceiver")
            if (resultCode == Activity.RESULT_OK) {
                val resultValue = intent.getStringExtra("resultValue")
                Toast.makeText(baseContext, resultValue, Toast.LENGTH_SHORT).show()
            }
        }
    }
}

