package com.example.sarge.uielements.first

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.example.sarge.uielements.R
import kotlinx.android.synthetic.main.activity_main.*
import java.io.*


/**
 * Created by sarge on 17.01.2017.
 */

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        secondReceiver.setOnClickListener {
            startService(Intent(this, KoltinService::class.java).putExtra("type", "delete"))
        }

        button3.setOnClickListener {
            startService(Intent(this, KoltinService::class.java).putExtra("type", "new"))
        }

        button2.setOnClickListener {
            startService(Intent(this, KoltinService::class.java).putExtra("type", "size"))
        }

        button4.setOnClickListener {
            write()
        }

        button6.setOnClickListener {
            textView.text = read()
        }

        button5.setOnClickListener {
            deleteFile("test")
        }

    }

    fun write() {
        Log.d("Logos", "write begin")
        val fos : FileOutputStream = openFileOutput("test", Context.MODE_APPEND)
        val writer = BufferedWriter(OutputStreamWriter(fos))
        writer.write("Hi, I'm writing stuff")
        writer.close()
        Log.d("Logos", "write end")
    }

    fun read(): String {
        Log.d("Logos", "read begin")
        val buffer = StringBuffer()
        try {
            val input : BufferedReader = BufferedReader(InputStreamReader(openFileInput("test")))
            input.forEachLine { line ->
                buffer.append(line + "\n")
                Log.d("Logos", "line " + line)
            }
            input.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        Log.d("Logos", "read end")
        return buffer.toString()
    }

}
