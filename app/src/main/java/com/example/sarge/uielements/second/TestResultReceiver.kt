package com.example.sarge.uielements.second

import android.os.Bundle
import android.os.Handler
import android.support.v4.os.ResultReceiver


/**
 * Created by sarge on 19.01.2017.
 */

class TestResultReceiver(handler: Handler?): ResultReceiver(handler) {

    var receiver: Receiver? = null

    interface Receiver {
        fun onReceiveResult(resultCode: Int, resultData: Bundle?)
    }

    override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
           receiver?.onReceiveResult(resultCode, resultData)
    }
}
