package com.example.sarge.uielements.entity

import android.util.Log
import com.activeandroid.Model
import com.activeandroid.annotation.Column
import com.activeandroid.annotation.Table
import com.activeandroid.query.Delete
import com.activeandroid.query.Select
import java.util.concurrent.TimeUnit


/**
 * Created by sarge on 18.01.2017.
 */

@Table(name = "Items")
open class Item(@Column(name = "Name") var name: String = "Default",
                @Column(name = "Age") var age: String = "Default",
                @Column(name = "Sex") var sex: String = "Default") : Model() {

    companion object {
        fun deleteAll() {
            Log.d("Logos", "DeleteStart")
            val a = Select().from(Item::class.java).execute<Item>()
            for (it in a) {
                it.delete()
                Log.d("Logos", "Delete ${it.name}")
            }
            Log.d("Logos", "DeleteEnd")
        }

        fun getDbSize() {
            Log.d("Logos", "SelectStart")
            val a = Select().from(Item::class.java).execute<Item>()
            Log.d("Logos", "Select ${a.size}")
        }
    }

}

