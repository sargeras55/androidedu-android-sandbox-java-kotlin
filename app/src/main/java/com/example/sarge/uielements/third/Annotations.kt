package com.example.sarge.uielements.third

/**
 * Created by sarge on 22.01.2017.
 */

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class onClick(val res: Int)
