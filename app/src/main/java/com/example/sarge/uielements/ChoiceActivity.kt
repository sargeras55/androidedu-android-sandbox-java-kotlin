package com.example.sarge.uielements

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.sarge.uielements.first.MainActivity
import com.example.sarge.uielements.second.SecondActivity
import com.example.sarge.uielements.third.AnnotationActivity
import kotlinx.android.synthetic.main.activity_choice.*

class ChoiceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choice)

        buttonFirst.setOnClickListener { startActivity(Intent(this, MainActivity::class.java)) }

        buttonSecond.setOnClickListener { startActivity(Intent(this, SecondActivity::class.java)) }

        annotations.setOnClickListener { startActivity(Intent(this, AnnotationActivity::class.java)) }
    }


}
