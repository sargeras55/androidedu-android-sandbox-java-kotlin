package com.example.sarge.uielements.third

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.sarge.uielements.R
import kotlinx.android.synthetic.main.activity_annotation.*

class AnnotationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_annotation)
        bindViewsFromActivity(this)
    }

    @onClick(R.id.mBtnAnnotationExecutorOne)
    fun showTitleOne() {
        mTvAnnotationTitle.text = "One"
    }

    @onClick(R.id.mBtnAnnotationExecutorTwo)
    fun showTitleTwo() {
        mTvAnnotationTitle.text = "Two"
    }
}
