package com.example.sarge.uielements;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

/**
 * Created by sarge on 18.01.2017.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
    }
}
